import { useRef, useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { store } from "../../backend";
import { addFarm } from "../../backend/entities/farm";
import { useSnapshot } from "valtio";

export function New() {
  const ref = useRef(uuidv4());
  return (
    <div className="home">
      <NavLink
        to={ref.current}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        Nouvelle Exploitation
      </NavLink>
    </div>
  );
}

export function Home() {
  const snap = useSnapshot(store);
  const location = useLocation();
  document.cookie = `id=${location.pathname.split("/")[1]}`;
  const [farmName, setFarmName] = useState("");
  if (store.farm === undefined) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          gap: "20px",
        }}
      >
        <h1>Quel est le nom de votre exploitation ?</h1>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <input
            value={farmName}
            onChange={(e) => {
              setFarmName(e.target.value);
            }}
            style={{
              fontSize: "2em",
              textAlign: "center",
              padding: "20px",
              width: "100%",
            }}
          />
          <button
            onClick={() => {
              addFarm({ title: farmName });
            }}
            onKeyDown={() => {}}
            style={{
              fontSize: "2em",
              textAlign: "center",
              padding: "20px",
            }}
          >
            Baptiser
          </button>
        </div>
      </div>
    );
  } else {
    return (
      <div className="home">
        <div>Bienvenue à {store.farm.title}</div>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <NavLink to="crews">
            {store.crews.length} Collègue{store.crews.length > 1 ? "s" : ""}
          </NavLink>
          <NavLink to="fields">
            {store.fields.length} Parcelle{store.fields.length > 1 ? "s" : ""}
          </NavLink>
          <NavLink to="products">
            {store.products.length} Produit
            {store.products.length > 1 ? "s" : ""}
          </NavLink>
          <NavLink to="clients">
            {store.clients.length} Client{store.clients.length > 1 ? "s" : ""}
          </NavLink>
        </div>
      </div>
    );
  }
}
