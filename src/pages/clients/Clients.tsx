import { useEffect, useState } from "react";
import { useSnapshot } from "valtio";
import {
  Client,
  addClient,
  deleteClient,
  store,
  updateClient,
} from "../../backend";
import {
  CatalogCard,
  CatalogDetail,
  CatalogList,
  CatalogueLayout,
} from "../../components/Catalog/Catalog";
import { TextLabelInput } from "../../components/form/TextInput";

export function Clients() {
  const [selected, setSelected] = useState<Client>();
  const snap = useSnapshot(store);

  useEffect(() => {
    const updated = store.clients.find((p) => p.id === selected?.id);
    if (updated) {
      setSelected(updated);
    }
  }, [snap]);

  return (
    <CatalogueLayout>
      <CatalogList
        title="Mes Clients"
        onSave={(title: string) => addClient({ title })}
      >
        {store.clients.map((entity) => (
          <CatalogCard
            key={entity.title}
            label={`${entity.title} `}
            selected={selected?.id === entity.id}
            onClick={() => setSelected(entity)}
            onDelete={() => deleteClient(entity)}
          />
        ))}
      </CatalogList>
      <CatalogDetail
        onUpdate={() => selected && updateClient({ ...selected })}
        show={!!selected}
        onClear={() => setSelected(undefined)}
      >
        {selected && (
          <>
            <TextLabelInput
              value={selected.title}
              label="Titre"
              onChange={(e) =>
                setSelected({ ...selected, title: e.target.value })
              }
            />
          </>
        )}
      </CatalogDetail>
    </CatalogueLayout>
  );
}
