import { useEffect, useState } from "react";
import { useSnapshot } from "valtio";
import { Crew, addCrew, deleteCrew, store, updateCrew } from "../../backend";
import {
  CatalogCard,
  CatalogDetail,
  CatalogList,
  CatalogueLayout,
} from "../../components/Catalog/Catalog";
import { TextLabelInput } from "../../components/form/TextInput";

export function Crews() {
  const [selected, setSelected] = useState<Crew>();
  const snap = useSnapshot(store);

  useEffect(() => {
    const updated = store.crews.find((p) => p.id === selected?.id);
    if (updated) {
      setSelected(updated);
    }
  }, [snap]);

  return (
    <CatalogueLayout>
      <CatalogList
        title="Mon Équipe"
        onSave={(title: string) => addCrew({ title })}
      >
        {store.crews.map((entity) => (
          <CatalogCard
            key={entity.title}
            label={`${entity.title}`}
            selected={selected?.id === entity.id}
            onClick={() => setSelected(entity)}
            onDelete={() => deleteCrew(entity)}
          />
        ))}
      </CatalogList>
      <CatalogDetail
        onUpdate={() => selected && updateCrew({ ...selected })}
        show={!!selected}
        onClear={() => setSelected(undefined)}
      >
        {selected && (
          <>
            <TextLabelInput
              value={selected.title}
              label="Titre"
              onChange={(e) =>
                setSelected({ ...selected, title: e.target.value })
              }
            />
          </>
        )}
      </CatalogDetail>
    </CatalogueLayout>
  );
}
