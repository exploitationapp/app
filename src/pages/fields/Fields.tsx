import { useEffect, useState } from "react";
import { useSnapshot } from "valtio";
import {
  Field,
  addField,
  deleteField,
  store,
  updateField,
} from "../../backend";
import {
  CatalogCard,
  CatalogDetail,
  CatalogList,
  CatalogueLayout,
} from "../../components/Catalog/Catalog";
import { TextLabelInput } from "../../components/form/TextInput";

export function Fields() {
  const [selected, setSelected] = useState<Field>();
  const snap = useSnapshot(store);

  useEffect(() => {
    const updated = store.fields.find((p) => p.id === selected?.id);
    if (updated) {
      setSelected(updated);
    }
  }, [snap]);

  return (
    <CatalogueLayout>
      <CatalogList
        title="Mes Parcelles"
        onSave={(title: string) => addField({ title })}
      >
        {store.fields.map((entity) => (
          <CatalogCard
            key={entity.id}
            label={`${entity.title}`}
            selected={selected?.id === entity.id}
            onClick={() => setSelected(entity)}
            onDelete={() => deleteField(entity)}
          />
        ))}
      </CatalogList>
      <CatalogDetail
        onUpdate={() => selected && updateField({ ...selected })}
        show={!!selected}
        onClear={() => setSelected(undefined)}
      >
        {selected && (
          <>
            <TextLabelInput
              value={selected.title}
              label="Titre"
              onChange={(e) =>
                setSelected({ ...selected, title: e.target.value })
              }
            />
          </>
        )}
      </CatalogDetail>
    </CatalogueLayout>
  );
}
