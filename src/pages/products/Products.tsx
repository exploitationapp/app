import { useEffect, useState } from "react";
import { useSnapshot } from "valtio";
import {
  Product,
  addProduct,
  deleteProduct,
  store,
  updateProduct,
} from "../../backend";
import {
  CatalogCard,
  CatalogDetail,
  CatalogList,
  CatalogueLayout,
} from "../../components/Catalog/Catalog";
import { NumberLabelInput } from "../../components/form/NumberInput";
import { TextLabelInput } from "../../components/form/TextInput";
import { ProductTaskForm } from "./ProductTaskForm";

export function Products() {
  const [selected, setSelected] = useState<Product>();
  const snap = useSnapshot(store);

  useEffect(() => {
    const updated = store.products.find((p) => p.id === selected?.id);
    if (updated) {
      setSelected(updated);
    }
  }, [snap]);

  return (
    <CatalogueLayout>
      <CatalogList
        title="Mes Produits"
        onSave={(title: string) => addProduct({ title })}
      >
        {store.products.map((entity) => (
          <CatalogCard
            key={entity.id}
            label={`${entity.title} - ${entity.price || 0}€ - ${
              entity.tasks?.length || 0
            } ${`tache${(entity.tasks?.length || 0) > 1 ? "s" : ""}`}`}
            selected={selected?.id === entity.id}
            onClick={() => setSelected(entity)}
            onDelete={() => deleteProduct(entity)}
          />
        ))}
      </CatalogList>
      <CatalogDetail
        onUpdate={() => selected && updateProduct({ ...selected })}
        show={!!selected}
        onClear={() => setSelected(undefined)}
      >
        {selected && (
          <div
            style={{ display: "flex", flexDirection: "column", gap: "50px" }}
          >
            <ProductTaskForm selected={selected} />
            <div>
              <TextLabelInput
                value={selected.title}
                label="Titre"
                onChange={(e) =>
                  setSelected({ ...selected, title: e.target.value })
                }
              />
              <NumberLabelInput
                value={selected.price || 0}
                label="Prix"
                onChange={(e) =>
                  setSelected({ ...selected, price: +e.target.value })
                }
              />
              <NumberLabelInput
                value={selected.begin || 0}
                label="Début"
                onChange={(e) =>
                  setSelected({ ...selected, begin: +e.target.value })
                }
              />
              <NumberLabelInput
                value={selected.end || 0}
                label="Fin"
                onChange={(e) =>
                  setSelected({ ...selected, end: +e.target.value })
                }
              />
            </div>
          </div>
        )}
      </CatalogDetail>
    </CatalogueLayout>
  );
}
