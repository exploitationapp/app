import { useState } from "react";
import { Product } from "../../backend";
import { deleteTask, addTask } from "../../backend/entities/task";
import { TextLabelInput } from "../../components/form/TextInput";
import { NumberLabelInput } from "../../components/form/NumberInput";

export function ProductTaskForm({ selected }: { selected: Product }) {
  const [newTask, setNewTask] = useState<{ title: string; offset: number }>({
    title: "",
    offset: 0,
  });
  return (
    <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
      {selected.tasks
        ?.sort((a, b) => (a.offset || 0) - (b.offset || 0))
        .map((task) => (
          <div
            key={task.id}
            onClick={() => deleteTask(task)}
            onKeyDown={() => {}}
            style={{
              padding: "5px 20px",
              border: "1px solid grey",
              userSelect: "none",
              cursor: "pointer",
              borderRadius: "5px",
            }}
          >
            S+{task.offset} - {task.title}
          </div>
        ))}
      <div>
        <NumberLabelInput
          value={newTask.offset}
          label="S+"
          onChange={(e) => setNewTask({ ...newTask, offset: +e.target.value })}
          min={0}
        />
        <TextLabelInput
          value={newTask.title}
          label="Titre"
          onChange={(e) => setNewTask({ ...newTask, title: e.target.value })}
        />
      </div>

      <button
        onClick={() => {
          addTask({
            ...newTask,
            product: selected.id,
          });
          setNewTask({ title: "", offset: 0 });
        }}
      >
        Ajouter une tâche
      </button>
    </div>
  );
}
