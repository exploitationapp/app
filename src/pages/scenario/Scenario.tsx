import { useSnapshot } from "valtio";
import { Product, store } from "../../backend";
import { useState } from "react";
import { ScenarioProductCard } from "../../components/Scenario/ScenarioProductCard";
import { ScenarioTimeline } from "../../components/Scenario/ScenarioTimeline";
import { ScenarioFieldLine } from "../../components/Scenario/ScenarioFieldLine";
import { addSeries, deleteSeries } from "../../backend/entities/series";

export function Scenario() {
  const snap = useSnapshot(store);
  const [selected, setSelected] = useState<Product>();

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "800px",
        gap: "20px",
      }}
    >
      <div>Planification</div>
      <div style={{ display: "flex", gap: "30px" }}>
        <div style={{ display: "flex", flexDirection: "column", gap: "10px" }}>
          <div>Produits</div>
          {store.products
            .filter((product) => product.begin != null || product.end != null)
            .map((product) => (
              <ScenarioProductCard
                product={product}
                selected={selected?.id === product.id}
                onClick={() => setSelected(product)}
              />
            ))}
        </div>
        <div style={{ flexGrow: 1 }}>
          <div>Parcelles</div>
          <ScenarioTimeline />
          {store.fields.map((field) => (
            <div key={field.id}>
              <div>{field.title}</div>
              <ScenarioFieldLine
                selected={selected}
                seriesList={store.seriesList.filter(
                  (series) => series.field === field.id
                )}
                onSave={() => {
                  selected &&
                    addSeries({
                      begin: selected.begin!,
                      end: selected.end!,
                      field: field.id,
                      fieldDoc: field,
                      product: selected.id!,
                      productDoc: selected,
                    });
                }}
                onDelete={(plan) => {
                  deleteSeries(plan);
                }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
