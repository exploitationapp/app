import { useState } from "react";
import { store } from "../../backend";
import { updateFarm } from "../../backend/entities/farm";

export function Settings() {
  const [farmName, setFarmName] = useState(store.farm?.title);
  return (
    <div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <input
          value={farmName}
          onChange={(e) => {
            setFarmName(e.target.value);
          }}
          style={{
            fontSize: "2em",
            textAlign: "center",
            padding: "20px",
            width: "100%",
          }}
        />
        <button
          onClick={() => {
            if (!store.farm) return;
            updateFarm({ ...store.farm, title: farmName || "" });
          }}
          onKeyDown={() => {}}
          style={{
            fontSize: "2em",
            textAlign: "center",
            padding: "20px",
          }}
        >
          Rebaptiser
        </button>
      </div>
    </div>
  );
}
