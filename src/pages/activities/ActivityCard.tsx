import { Activity } from "../../backend";
import { store } from "../../backend";

export function ActivityCard({
  activity,
  selected,
  onClick,
}: {
  activity: Activity;
  selected: boolean;
  onClick: () => void;
}) {
  return (
    <div
      style={{
        border: "1px solid orange",
        borderRadius: "5px",
        padding: "5px 0",
        cursor: "pointer",
        userSelect: "none",
        backgroundColor: selected ? "grey" : "black",
      }}
      onClick={onClick}
      onKeyDown={() => {}}
    >
      <div>{activity.title}</div>
      <div>{activity.productDoc?.title}</div>
      <div>{activity.fieldDoc?.title}</div>
      <div>
        {activity.crew
          ? store.crews.find((crew) => activity.crew === crew.id)?.title || ""
          : "non attribuée"}
      </div>
    </div>
  );
}
