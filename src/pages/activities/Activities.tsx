import { useState } from "react";
import { Crew, store } from "../../backend";
import { useSnapshot } from "valtio";
import { Activity, updateActivity } from "../../backend/entities/activities";
import { ActivityCard } from "./ActivityCard";

export function Activities() {
  const [selectedCrew, setSelectedCrew] = useState<Crew>();
  const [selectedWeek, setSelectedWeek] = useState<number>(1);
  const [selectedActivity, setSelectedActivity] = useState<Activity>();
  const snap = useSnapshot(store);

  return (
    <div>
      <h1>Activités</h1>
      <div className="flex flex-col gap-3">
        <div className="flex gap-2 align-middle">
          <div className="flex gap-1 flex-wrap max-w-60 justify-start">
            {store.crews.map((crew) => (
              <div
                className="cursor-pointer user-select-none text-center border-1 border-grey p-0.5 rounded-md"
                style={{
                  backgroundColor:
                    selectedCrew?.id === crew.id ? "grey" : "black",
                }}
                onClick={() => setSelectedCrew(crew)}
                onKeyDown={() => {}}
              >
                {crew.title}
              </div>
            ))}
          </div>
          <div className="flex">
            {Array.from(Array(10).keys()).map((i) => (
              <div
                style={{
                  border: "1px solid grey",
                  width: "40px",
                  textAlign: "center",
                  cursor: "pointer",
                  userSelect: "none",
                  backgroundColor: selectedWeek === i + 1 ? "grey" : "black",
                  color:
                    store.activities.filter(
                      (activity) =>
                        activity.week === i + 1 && activity.day == null
                    ).length === 0
                      ? "white"
                      : "orange",
                }}
                onClick={() => {
                  setSelectedWeek(i + 1);
                  setSelectedActivity(undefined);
                }}
                onKeyDown={() => {}}
              >
                {i + 1}
              </div>
            ))}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            width: "100%",
            padding: "5px",
            gap: "5px",
          }}
        >
          <div
            style={{
              flexGrow: 1,
              textAlign: "center",
            }}
          >
            <div style={{ border: "1px solid yellow", borderRadius: "10px" }}>
              TODO
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                gap: "5px",
                padding: "5px",
              }}
            >
              {store.activities
                .filter(
                  (activity) =>
                    activity.week === selectedWeek && activity.day == null
                )
                .map((activity) => (
                  <ActivityCard
                    activity={activity}
                    selected={selectedActivity?.id === activity.id}
                    onClick={() => setSelectedActivity(activity)}
                  />
                ))}
              {selectedWeek &&
                selectedActivity &&
                selectedActivity.day != null && (
                  <button
                    onClick={() => {
                      setSelectedActivity(undefined);
                      updateActivity({
                        ...selectedActivity,
                        day: undefined,
                        crew: undefined,
                      });
                    }}
                  >
                    -
                  </button>
                )}
            </div>
          </div>
          {["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi"].map(
            (dayName, day) => (
              <div
                style={{
                  flexGrow: 1,
                  textAlign: "center",
                }}
              >
                <div style={{ border: "1px solid grey", borderRadius: "5px" }}>
                  {dayName}
                </div>
                <div>
                  {store.activities
                    .filter(
                      (activity) =>
                        activity.day === day && activity.week === selectedWeek
                    )
                    .map((activity) => (
                      <ActivityCard
                        activity={activity}
                        selected={selectedActivity?.id === activity.id}
                        onClick={() => setSelectedActivity(activity)}
                      />
                    ))}
                  {selectedWeek && selectedActivity && selectedCrew && (
                    <button
                      onClick={() => {
                        setSelectedActivity(undefined);
                        updateActivity({
                          ...selectedActivity,
                          day,
                          crew: selectedCrew.id,
                        });
                      }}
                    >
                      +
                    </button>
                  )}
                </div>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
}
