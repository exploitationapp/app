import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Clients } from "./pages/clients/Clients";
import { Crews } from "./pages/crews/Crews";
import { Fields } from "./pages/fields/Fields";
import { Products } from "./pages/products/Products";
import App from "./App";
import { Home, New } from "./pages/home/Home";
import { Scenario } from "./pages/scenario/Scenario";
import { Activities } from "./pages/activities/Activities";
import { Manuel } from "./pages/Manuel/Manuel";
import { Settings } from "./pages/settings/Settings";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route path="/" element={<New />} />
          <Route path=":id/" element={<Home />} />
          <Route path=":id/products" element={<Products />} />
          <Route path=":id/clients" element={<Clients />} />
          <Route path=":id/fields" element={<Fields />} />
          <Route path=":id/crews" element={<Crews />} />
          <Route path=":id/scenario" element={<Scenario />} />
          <Route path=":id/activities" element={<Activities />} />
          <Route path=":id/manuel" element={<Manuel />} />
          <Route path=":id/settings" element={<Settings />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
