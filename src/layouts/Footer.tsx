export function Footer() {
  return (
    <div className="footer">
      <div>
        Retrouvez le code source en code libre sur{" "}
        <a
          href="https://gitlab.com/exploitationapp/app"
          target="_blank"
          rel="noreferrer"
        >
          Gitlab
        </a>
      </div>
    </div>
  );
}
