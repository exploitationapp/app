import { Footer } from "./Footer";
import { Header } from "./Header";
import "./style.css";

export function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <div className="main-layout">
      <Header />
      <div className="content">{children}</div>

      <Footer />
    </div>
  );
}
