import { Client, Crew, Field, Product } from "../entities";
import { proxy } from "valtio";
import { Series } from "../entities/series";
import { Task } from "../entities/task";
import { Activity } from "../entities/activities";
import { Farm } from "../entities/farm";

export const store = proxy<{
  products: Product[];
  clients: Client[];
  fields: Field[];
  crews: Crew[];
  seriesList: Series[];
  tasks: Task[];
  activities: Activity[];
  farm: Farm | undefined;
}>({
  products: [],
  clients: [],
  fields: [],
  crews: [],
  seriesList: [],
  tasks: [],
  activities: [],
  farm: undefined,
});

export type StoreKeys = keyof typeof store;
