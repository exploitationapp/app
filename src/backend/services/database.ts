import PouchDb from "pouchdb";
import find from "pouchdb-find";
import rel from "relational-pouch";
import {
  loadActivities,
  loadClients,
  loadCrews,
  loadFarm,
  loadFields,
  loadProducts,
  loadSeriesList,
} from "../entities";

PouchDb.plugin(find).plugin(rel);

export let db: PouchDB.Database<{}>;
export let relDB: PouchDB.RelDatabase<{}>;
let previousLocation: string;
export function startCloudSync(location: string) {
  if (!location || previousLocation === location) return;
  previousLocation = location;
  db = new PouchDb(location);
  var url = `https://db.exploitation.app/${location}`;
  var opts = { live: true, retry: true };

  db.replicate
    .from(url)
    .on("complete", function (info) {
      // then two-way, continuous, retriable sync
      db.sync(url, opts)
        .on("change", console.log)
        .on("paused", console.log)
        .on("error", console.log);
    })
    .on("error", console.log);

  relDB = db.setSchema([
    {
      singular: "product",
      plural: "products",
      relations: {
        seriesList: { hasMany: "series" },
        tasks: {
          hasMany: { type: "task", options: { queryInverse: "product" } },
        },
      },
    },
    {
      singular: "task",
      plural: "tasks",
      relations: { product: { belongsTo: "product" } },
    },
    {
      singular: "client",
      plural: "clients",
    },
    {
      singular: "field",
      plural: "fields",
      relations: {
        seriesList: {
          hasMany: {
            type: "series",
            options: { queryInverse: "field" },
          },
        },
      },
    },
    {
      singular: "crew",
      plural: "crews",
    },
    {
      singular: "series",
      plural: "seriesList",
      relations: {
        product: { belongsTo: "product" },
        field: { belongsTo: "field" },
      },
    },
    {
      singular: "activity",
      plural: "activities",
      relations: {
        series: { belongsTo: "series" },
        task: { belongsTo: "task" },
        product: { belongsTo: "product" },
        field: { belongsTo: "field" },
      },
    },
    {
      singular: "farm",
      plural: "farms",
      relations: { products: { hasMany: "product" } },
    },
  ]);

  relDB.createIndex({ index: { fields: ["data.product", "_id"] } });
  relDB.createIndex({ index: { fields: ["data.field", "_id"] } });

  loadFarm();
  loadProducts();
  loadClients();
  loadFields();
  loadCrews();
  loadSeriesList();
  loadActivities();
  let timer: number;
  db.changes({
    since: "now",
    live: true,
  }).on("change", function (change) {
    console.log({ change });
    clearTimeout(timer);
    timer = setTimeout(() => {
      loadFarm();
      loadProducts();
      loadClients();
      loadFields();
      loadCrews();
      loadSeriesList();
      loadActivities();
    }, 100);
  });
}

// relDB.rel
//   .save("farm", {
//     name: "Ferme sans nom",
//     id: 1,
//     products: [1, 2, 3],
//   })
//   .then(function () {
//     return relDB.rel.save("product", {
//       title: "Carotte",
//       id: 1,
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("product", {
//       title: "Aubergine",
//       id: 2,
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("product", {
//       title: "Salade",
//       id: 3,
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("client", {
//       title: "Biocoop",
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("client", {
//       title: "Cantine de l'école",
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("field", {
//       title: "Parcelle 1",
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("field", {
//       title: "Parcelle 2",
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("crew", {
//       title: "Michel",
//     });
//   })
//   .then(function () {
//     return relDB.rel.save("crew", {
//       title: "Patrick",
//     });
//   })
//   .catch(console.log.bind(console));
