import { InputValue } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Field = {
  id: string;
  title: string;
};

export async function loadFields() {
  const fields = await relDB.rel.find("field");
  const fields_1 = fields.fields.sort((a: Field, b: Field) => {
    return a.title.localeCompare(b.title);
  });
  return (store.fields = fields_1);
}

export const addField = (field: InputValue<Field>) => {
  if (field.title === "") return;
  relDB.rel.save("field", field);
};

export const updateField = (field: Field) => {
  relDB.rel.save("field", field);
};

export const deleteField = (field: Field) => {
  relDB.rel.del("field", field);
};
