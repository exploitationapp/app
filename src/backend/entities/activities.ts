import { Field, InputValue, Product, Series } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Activity = {
  id: string;
  title: string;
  series: string;
  product: string;
  field: string;
  day?: number;
  week?: number;
  productDoc?: Product;
  fieldDoc?: Field;
  crew?: string;
};

export async function loadActivities() {
  const result = await relDB.rel.find("activity");
  const activity_1 = result.activities;
  store.activities = activity_1;
}

export const addActivity = (activity: InputValue<Activity>) => {
  relDB.rel.save("activity", activity);
};

export const updateActivity = (activity: Activity) => {
  relDB.rel.save("activity", activity);
};

export const deleteActivity = (activity: Activity) => {
  relDB.rel.del("activity", activity);
};
