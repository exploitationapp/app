import { Activity, Field, InputValue, Product } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Series = {
  id: string;
  begin: number;
  end: number;
  product: string;
  productDoc: Product;
  field: string;
  fieldDoc: Field;
};

export async function loadSeriesList() {
  const result = await relDB.rel.find("series");
  const seriesList_1 = result.seriesList;
  store.seriesList = seriesList_1;
}

export const addSeries = (series: InputValue<Series>) => {
  relDB.rel.save("seriesList", series).then((seriesResult) => {
    series.productDoc.tasks?.forEach((task) => {
      relDB.rel.save("activity", {
        series: seriesResult.id,
        title: task.title,
        week: (series.begin || 0) + (task.offset || 0),
        product: series.product,
        productDoc: series.productDoc,
        field: series.field,
        fieldDoc: series.fieldDoc,
      });
    });
  });
};

export const updateSeries = (series: Series) => {
  relDB.rel.save("series", series);
};

export const deleteSeries = (series: Series) => {
  relDB.rel.del("series", series);
  relDB.rel.find("activity").then((result) => {
    const activities: Activity[] = result.activities;
    activities
      .filter((activity) => activity.series === series.id)
      .forEach((activity) => {
        relDB.rel.del("activity", activity);
      });
  });
};
