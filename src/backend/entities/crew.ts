import { InputValue } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Crew = {
  id: string;
  title: string;
};

export async function loadCrews() {
  const crews = await relDB.rel.find("crew");
  const crews_1 = crews.crews.sort((a: Crew, b: Crew) => {
    return a.title.localeCompare(b.title);
  });
  store.crews = crews_1;
}

export const addCrew = (crew: InputValue<Crew>) => {
  if (crew.title === "") return;
  relDB.rel.save("crew", crew);
};

export const updateCrew = (crew: Crew) => {
  relDB.rel.save("crew", crew);
};

export const deleteCrew = (crew: Crew) => {
  relDB.rel.del("crew", crew);
};
