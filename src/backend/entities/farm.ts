import { InputValue } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Farm = {
  id: string;
  title: string;
};

export async function loadFarm() {
  const result = await relDB.rel.find("farm");
  store.farm = result.farms[0];
}

export const addFarm = (farm: InputValue<Farm>) => {
  relDB.rel.save("farm", farm);
};

export const updateFarm = (farm: Farm) => {
  relDB.rel.save("farm", farm);
};

export const deleteFarm = (farm: Farm) => {
  relDB.rel.del("farm", farm);
};
