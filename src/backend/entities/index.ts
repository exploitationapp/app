export * from "./activities";
export * from "./client";
export * from "./crew";
export * from "./farm";
export * from "./field";
export * from "./product";
export * from "./series";

export type InputValue<T> = Omit<T, "id">;
