import { InputValue } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";

export type Client = {
  id: string;
  title: string;
};

export async function loadClients() {
  const clients = await relDB.rel.find("client");
  const clients_1 = clients.clients.sort((a: Client, b: Client) => {
    return a.title.localeCompare(b.title);
  });
  return (store.clients = clients_1);
}

export const addClient = (client: InputValue<Client>) => {
  if (client.title === "") return;
  relDB.rel.save("client", client);
};

export const updateClient = (client: Client) => {
  relDB.rel.save("client", client);
};

export const deleteClient = (client: Client) => {
  relDB.rel.del("client", client);
};
