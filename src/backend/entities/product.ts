import { InputValue } from ".";
import { relDB } from "../services/database";
import { store } from "../services/store";
import { Task } from "./task";

export type Product = {
  id: string;
  title: string;
  price?: number;
  begin?: number;
  end?: number;
  tasks?: Task[];
};

export async function loadProducts() {
  const result = await relDB.rel.find("product");
  const products_1 = result.products.sort((a: Product, b: Product) => {
    return a.title.localeCompare(b.title);
  });
  const tasks_1 = result.tasks;

  store.products = products_1.map((product: Product) => ({
    ...product,
    tasks: tasks_1.filter((task: Task) => task.product === product.id),
  }));
}

export const addProduct = (product: InputValue<Product>) => {
  if (product.title === "") return;
  relDB.rel.save("product", product);
};

export const updateProduct = (product: Product) => {
  relDB.rel.save("product", product);
};

export const deleteProduct = (product: Product) => {
  relDB.rel.del("product", product);
};
