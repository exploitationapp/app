import { InputValue, Product } from ".";
import { relDB } from "../services/database";

export type Task = {
  id: string;
  title: string;
  product: string;
  offset?: number;
  productDoc?: Product;
  period?: number;
};

export const addTask = (el: InputValue<Task>) => {
  relDB.rel.save("task", el);
};

export const updateTask = (el: Task) => {
  relDB.rel.save("task", el);
};

export const deleteTask = (el: Task) => {
  relDB.rel.del("task", el);
};
