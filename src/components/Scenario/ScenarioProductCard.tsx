import { Product } from "../../backend";

export function ScenarioProductCard({
  product,
  selected,
  onClick,
}: {
  product: Product;
  selected: boolean;
  onClick: () => void;
}) {
  return (
    <div
      onClick={onClick}
      onKeyDown={() => {}}
      style={{
        backgroundColor: selected ? "transparent" : "darkgrey",
        cursor: "pointer",
        userSelect: "none",
        border: "1px solid grey",
        padding: "10px",
        borderRadius: "10px",
      }}
      key={product.id}
    >
      {product.title}
      {product.begin && product.end
        ? ` - ${product.begin}...${product.end}`
        : null}
    </div>
  );
}
