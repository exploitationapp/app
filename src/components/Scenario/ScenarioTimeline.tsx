import { store } from "../../backend";

export function ScenarioTimeline() {
  const work = store.seriesList.reduce((memo: number[], plan) => {
    plan.productDoc?.tasks?.forEach((task) => {
      const index = (task?.offset || 0) + (plan.productDoc?.begin || 0) - 1;
      if (task.offset != null && !memo[index]) {
        memo[index] = 0;
      }

      if (task.offset != null) {
        memo[index]++;
      }
    });
    return memo;
  }, Array.from(Array(10)));

  return (
    <div style={{ display: "flex", border: "1px solid black" }}>
      {work.splice(0, 10).map((value, i) => (
        <div
          style={{
            flexGrow: 1,
            border: "1px solid orange",
            textAlign: "center",
            width: "10%",
          }}
        >
          {i + 1} : {value}
        </div>
      ))}
    </div>
  );
}
