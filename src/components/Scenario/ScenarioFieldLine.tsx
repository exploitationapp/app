import { useState } from "react";
import { Product } from "../../backend";
import { Series } from "../../backend/entities/series";
import { stringToColor } from "../../utils/colors";
import "./style.css";

const isAvailable = (selected: Product, seriesList: Series[]): boolean => {
  const begin = selected.begin!;
  const end = selected.end!;
  for (let i = begin; i <= end; i++) {
    if (seriesList.some((plan) => plan.begin <= i && plan.end >= i)) {
      return false;
    }
  }
  return true;
};

export function ScenarioFieldLine({
  selected,
  onSave,
  onDelete,
  seriesList,
}: {
  selected: Product | undefined;
  onSave: () => void;
  onDelete: (plan: Series) => void;
  seriesList: Series[];
}) {
  return (
    <div style={{ display: "flex" }}>
      <div
        style={{
          flexGrow: 1,
          border: "1px solid grey",
          borderRadius: "10px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          minHeight: "40px",
          position: "relative",
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            textAlign: "center",
            position: "relative",
            height: "100%",
            width: "98%",
          }}
        >
          {seriesList.map((plan) => (
            <div
              key={plan.id}
              className="scenario-select-field planification"
              style={{
                borderRadius: "10px",
                backgroundColor: stringToColor(plan.productDoc?.title || ""),
                position: "absolute",
                left: `${plan.begin - 1}0%`,
                width: `${plan.end - plan.begin + 1}0%`,
              }}
              onClick={() => onDelete(plan)}
              onKeyDown={() => {}}
            >
              {plan.productDoc?.title}
            </div>
          ))}
          {selected?.begin &&
            selected.end &&
            isAvailable(selected, seriesList) && (
              <ScenarioSelectField
                begin={selected.begin}
                end={selected.end}
                onClick={onSave}
                next={selected.title}
              />
            )}
        </div>
      </div>
    </div>
  );
}

function ScenarioSelectField({
  begin,
  end,
  onClick,
  next,
}: {
  begin: number;
  end: number;
  onClick: () => void;
  next: string;
}) {
  const [hover, setHover] = useState(false);

  return (
    <div
      onClick={onClick}
      onKeyDown={() => {}}
      className="scenario-select-field"
      style={{
        borderRadius: "10px",
        textAlign: "center",
        position: "absolute",
        left: `${begin - 1}0%`,
        width: `${end - begin + 1}0%`,
      }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      {hover ? next : "Disponible"}
    </div>
  );
}
