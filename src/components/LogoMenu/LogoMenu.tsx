import "./style.css";
import { NavLink, useLocation } from "react-router-dom";
import { useMemo, useState } from "react";

export function LogoMenu() {
  const [open, setOpen] = useState(false);
  let location = useLocation();

  const routes = useMemo(() => {
    const exploitationId = location.pathname.split("/")[1] || "";

    return [
      { path: `${exploitationId}/`, title: "Accueil" },
      { path: `${exploitationId}/crews`, title: "Équipe" },
      { path: `${exploitationId}/fields`, title: "Parcelles" },
      { path: `${exploitationId}/products`, title: "Produits" },
      { path: `${exploitationId}/clients`, title: "Clients" },
      { path: `${exploitationId}/scenario`, title: "Planif'" },
      { path: `${exploitationId}/activities`, title: "Activités" },
      { path: `${exploitationId}/manuel`, title: "Manuel" },
      { path: `${exploitationId}/settings`, title: "Réglages" },
    ];
  }, [location]);

  return (
    <div
      className={`logo-menu ${open && "logo-menu--show"}`}
      onClick={() => setOpen((open) => !open)}
      onKeyDown={() => {}}
    >
      {open && (
        <div className="logo-menu__content">
          {routes.map((route) => (
            <NavLink to={route.path} onClick={(e) => e.stopPropagation()}>
              <div className="logo-menu__card">{route.title}</div>
            </NavLink>
          ))}
        </div>
      )}
    </div>
  );
}
