import { useState } from "react";
import { ListItem, ListItems } from "../ListItems/ListItems";
import { TextInput } from "../form/TextInput";
import "./style.css";

export function CatalogueLayout({ children }: { children: React.ReactNode }) {
  return <div className="catalogue-layout">{children}</div>;
}

export function CatalogCard({
  label,
  onClick,
  selected,
  onDelete,
}: {
  label: string;
  onClick: () => void;
  onDelete: () => void;
  selected?: boolean;
}) {
  return (
    <ListItem>
      <div onClick={onClick} onKeyDown={() => {}}>
        {selected && ">>"} {label}
      </div>
      <button type="button" onClick={onDelete}>
        X
      </button>
    </ListItem>
  );
}

export function CatalogList({
  title,
  children,
  onSave,
}: {
  title: string;
  children: React.ReactNode;
  onSave: (newTitle: string) => void;
}) {
  const [newTitle, setNewTitle] = useState("");

  return (
    <div>
      <h1>{title}</h1>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <TextInput
          value={newTitle}
          onChange={(e) => setNewTitle(e.target.value)}
        />
        <button
          type="button"
          onClick={() => {
            onSave(newTitle);
            setNewTitle("");
          }}
        >
          +
        </button>
      </div>
      <ListItems>{children}</ListItems>
    </div>
  );
}

export function CatalogDetail({
  children,
  onUpdate,
  show,
  onClear,
}: {
  children: React.ReactNode;
  onUpdate: () => void;
  show: boolean;
  onClear: () => void;
}) {
  return (
    <div>
      {show && (
        <div
          style={{ display: "flex", flexDirection: "column", height: "100%" }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              marginBottom: "1rem",
            }}
          >
            <div>Détail</div>
            <button onClick={onClear}>X</button>
          </div>

          <div
            style={{ display: "flex", flexDirection: "column", flexGrow: 1 }}
          >
            {children}
          </div>
          <button onClick={onUpdate}>Enregistrer</button>
        </div>
      )}
    </div>
  );
}
