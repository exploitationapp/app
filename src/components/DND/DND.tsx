import { DndProvider, useDrag, useDrop } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

export function DND() {
  return (
    <DndProvider backend={HTML5Backend}>
      <div>DND</div>
      <Box />
      <Drop />
    </DndProvider>
  );
}

const ItemTypes = {
  BOX: "box",
};

function Box() {
  const [{ isDragging }, drag] = useDrag(() => ({
    type: ItemTypes.BOX,
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 25,
        fontWeight: "bold",
        cursor: "move",
      }}
    >
      ♘
    </div>
  );
}

function Drop() {
  const [, drop] = useDrop(() => ({
    accept: ItemTypes.BOX,
    drop: () => console.log("HELLO"),
  }));

  return (
    <div
      ref={drop}
      style={{ width: "300px", height: "300px", border: "1px solid red" }}
    >
      Drop here
    </div>
  );
}
