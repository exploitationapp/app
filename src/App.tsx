import { startCloudSync } from "./backend/services/database";
import { MainLayout } from "./layouts/MainLayout";
import { Outlet, useLocation } from "react-router-dom";

function App() {
  const location = useLocation();
  startCloudSync(location.pathname.split("/")[1]);
  return (
    <MainLayout>
      <Outlet />
    </MainLayout>
  );
}

export default App;
